from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView
from django.http import HttpResponse
from .models import Ship, Cargo, Remark, Information, Master
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from .forms import InfoForm
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse
from django.contrib.auth.models import User, Group
def home(request):
    try:
        ships = Ship.objects.all().order_by("-id")
        cargo = Cargo.objects.all().order_by("-id")
        
        remark = Remark.objects.all().order_by("-id")
    except:
        ships()
        cargo()
        remark()
 
    return render(request, 'home.html' , {'ships':ships, 'cargo':cargo, 'remark':remark})

def about(request):
    try:
        ships = Ship.objects.all().order_by("-id")
        cargo = Cargo.objects.all().order_by("-id")
        remark = Remark.objects.all().order_by("-id")
    except:
        ships()
        cargo()
        remark()
    return render(request, 'view.html',{'ships':ships, 'cargo':cargo, 'remark':remark})

def about_company(request):
  
    return render(request, 'about_company.html', {'company_name': 'Simple Complex'})

def ship_lastports(request,pk):
    try:
        
        ships = Ship.objects.get(pk=pk)
        
        
        information = Information.objects.get(pk=pk)
       
    except Ship.DoesNotExist:
        raise Http404
    return render(request, 'lastport.html', {'ships':ships, 'information':information, })

# def cargo(request, pk):
    
#     ship = get_object_or_404(Ship, pk=pk)
#     if request.method == 'POST':
#         cargo = request.POST['cargo']
#         message = request.POST['information']

#         user = User.objects.first()  

#         cargo = Cargo.objects.create(
#             cargo=cargo,
#             ship=ship,
#             starter=user
#         )

#         remark = Remark.objects.create(
#             message=message,
#             cargo=cargo,
#             created_by=user
#         )

#         return redirect('ship_lastport', pk=ship.pk)  

#     return render(request, 'newport.html', {'ship': ship})
@csrf_protect
def shipinformation(request):
    if request.method == "GET":
        try:
            master = Master.objects.all().order_by("id")
            ship = Ship.objects.all().order_by("-id")
            group = Group.objects.get(name='master')
            usersList = User.objects.all().order_by("-id")
        except:
            master()
            ship()
            usersList()
        return render(request, 'shipinfo.html', {'master':master, 'ship':ship, 'usersList':usersList})
    
    if request.method == 'POST':
        try:
            Information.objects.create(
                captain = request.POST.get("captain"),
                ship = request.POST.get("ship"),
                imo = request.POST.get("imo"),
                mmsi = request.POST.get("mmsi"),
                callsign = request.POST.get("callsign"),
                gross = request.POST.get("gross"),
                deadweight = request.POST.get("deadweight"),
                buildyear = request.POST.get("buildyear"))
            return  JsonResponse({"code":200,"data":None,"msg":"Added successfully"})
        except Exception as e:
            return  JsonResponse({"code":500,"data":str(e),"msg":"add failed"})  
   
        

    


def signup(request, pk):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})