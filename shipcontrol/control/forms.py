from django import forms
from .models import Information

class InfoForm(forms.ModelForm):
    captain = forms.CharField(max_length=50)
    ship = forms.CharField(max_length=50)
    imo = forms.CharField(max_length=10)
    mmsi = forms.CharField(max_length=10)
    callsign = forms.CharField(max_length=10)
    gross = forms.CharField(max_length=10)
    deadweight = forms.CharField(max_length=10)
    buildyear = forms.CharField(max_length=10)
    
    class Meta:
        model = Information
        fields = ('captain', 'ship','imo','mmsi','callsign','gross','deadweight','buildyear')
    def save(self):
        data = self.cleaned_data