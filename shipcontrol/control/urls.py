from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf.urls import url
from .models import Ship
from . import models



urlpatterns = [
    path('', views.home, name='home'),
    url(r'^about/$', views.about, name='about'),
    url(r'^about/company/$', views.about_company, name='about_company'),
    url(r'^ships/(?P<pk>\d+)/$', views.ship_lastports, name='ship_lastport'),
    url(r'^signup/$', views.signup, name='signup'),
   # url(r'^ships/(?P<pk>\d+)/new/$', views.cargo, name='cargo'),
    url(r'^infoedit/$', views.shipinformation, name='info'),
  #  url(r'^ships/(?P<pk>\d+)/$', views.ship_lastport, name='ship_lastport'),
]